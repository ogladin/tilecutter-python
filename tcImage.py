# 
#   tcImage.py
#   tilecutter
# 
#   Created by Michel Beaudouin-Lafon on 4/7/14.
#   Ported by Olivier Gladin, SED SACLAY, INRIA on January 2015
#   Copyright (c) 2014 Michel Beaudouin-Lafon. All rights reserved.
#   Using Wand Copyright (c) 2015 Hong Minhee, http://dahlia.kr/ <minhee@dahlia.kr>
#   Wand is a binding of ImageMagick http://www.imagemagick.org/



from wand.image import Image
from wand.color import Color
from os.path import splitext, join, getmtime
from calendar import timegm
from time import strptime

import sys


from tcParameters import ScaleType



# class tcImage(object):
# 
# 	def __init__(self):
# 		pass
	
	#Return the image type to use for saving based on the extension of the input file

def fileTypeForFile(imgFileName) :
	#*** We only generate png for now
	return "png"


#Return true if the file can be processed (based on its extension)
def isImageFile(imgFileName) :
							
	extension = splitext(imgFileName)[1]
	extension = extension.lower();

	if extension in ['png','pdf', 'jpg', 'jpeg', 'tif', 'tiff', 'bmp']: return True
	return False
	
#Load an image
def loadImage(imgFileName):
	return Image(filename=imgFileName,resolution=500)#changed default value to 500 pixels per inch




#Save an image to a file
def saveImage(img, fileName, noExec):
	#Get the bitmap representation
	
	if noExec:
		print ("Skipping file "+fileName + " (noexec option)")
		return True
	else:
		print ("Saving "+fileName)
		
		try:
			img.format = 'png'
			img.save(filename=fileName)
		except:
			print ("Error while saving "+fileName+"(" +str(sys.exc_info()[0])+")")
			return False
		
		return True





#Rotate an image by +/- 90 degrees
def rotateImage(img, angle):
	toRotate = img.clone()
	toRotate.rotate(angle)
	return toRotate


#add border to reach the parameter size
def extendImage(img, width, height, bgcolor):
	
	borderWidth = borderHeight = 0
	
	if img.width < width:
		borderWidth = (width-img.width)/2

	if img.height < height:
		borderHeight = (height-img.height)/2
	
	if borderWidth > 0 or borderHeight > 0:
		img.border(Color(bgcolor),borderWidth,borderHeight)
	
	return img


#Scale an image to the wall according to the type of scaling

def scaleToWall(img, wall, imageParams): 
	#Get params
	bgColor = imageParams.bgColor
	scale = imageParams.scaleType
	wallSize = wall.sizeWithBezels
	wallCrop = wall.crop

	if imageParams.ignoreBezels:
		wallSize = wall.sizeWithoutBezels
		

	if (imageParams.rotate != 0):
		print("Rotating ", str(imageParams.rotate))
		img = rotateImage(img, imageParams.rotate)


	imgWidth = wallSize['width']
	imgHeight = wallSize['height']
	
	leftCrop =  wallCrop['left']
	rightCrop =  wallCrop['right']
	topCrop =  wallCrop['top']
	bottomCrop =  wallCrop['bottom']
	
	imgWidth -= (leftCrop+rightCrop)
	imgHeight -= (topCrop+bottomCrop)
	
	if imgWidth <= 0:
		print ("Horizontal crop is greater then wall width.")
		return None
	elif imgHeight <= 0:	
		print ("Vertical crop is greater then wall height.")
		return None
		
	
	
	doExtend = True
	doCrop = False


	if scale == ScaleType.ScaleFit: #Scale the image so it fits but is not cropped
		resizeStr = str(imgWidth)+'x'+str(imgHeight)
		print("ScaleFit "+resizeStr)
		img.transform(resize=resizeStr)
	elif scale == ScaleType.ScaleFill: #Scale the image so it fills the wall (and gets cropped)
		resizeStr = str(imgWidth)+'x'+str(imgHeight)+"^"
		print("ScaleFill "+resizeStr)
		img.transform(resize=resizeStr)
		doCrop = True
	elif scale == ScaleType.ScaleMap:#map image to screen (with possible distortion)
		resizeStr = str(imgWidth)+'x'+str(imgHeight)+"!"
		print("ScaleMap "+resizeStr)
		img.transform(resize=resizeStr)
		doExtend = False #No need to extend, already stretched
	else:
		print("ScaleNone")

	
	#ScaleType.ScaleNone -> only extend
	
	if doExtend:
		img = extendImage(img, imgWidth, imgHeight, bgColor)
		
	if doCrop:
		img.crop((img.width-imgWidth)/2, (img.height-imgHeight)/2, width=imgWidth, height=imgHeight)
	
	return img


#Return the date of the oldest existing tile, and distantPast if one or more tiles are missing

def oldestTile(fileName, destDir, wall):
	
	oldest = timegm(strptime("31 Dec 2999", "%d %b %Y")) #was [NSDate distantFuture]


	for row in range(0,wall.numRows):
		for col in range(0,wall.numColumns):
			tileName = join(destDir,wall.tileNames[row][col] + ".png")
			try:
				tileDate = getmtime(tileName);
			except:
				tileDate = 0
			oldest = min (oldest, tileDate)
	return oldest    

#Return the date of the oldest existing thumbnail, and distantPast if one or more thumbnails are missing

def oldestThumbnail(fileName, destDir, numSizes, thumbSizes):
	oldest = timegm(strptime("31 Dec 2999", "%d %b %Y")) #was [NSDate distantFuture]

	for i in range(0, numSizes):   
		size = thumbSizes[i]
		thumbName = join(destDir,"thumb"+str(size)+".png")
		try:
			thumbDate = getmtime(thumbName);
		except:
			thumbDate = 0
		oldest = min(oldest,thumbDate)
	return oldest


#Create a tile from src image
def makeTile(imageSrc, row, col, wall, imageParams, fullPath, writeFiles):
	print ("makeTile "+ str(imageSrc) + " row " + str(row) + " col " + str(col))
	
	#Position of the tile in the image

	if (imageParams.ignoreBezels):
		x = col * wall.tileSize['width']
		y = row * wall.tileSize['height']
		width = wall.tileSize['width']
		height = wall.tileSize['height']
	else:
		x = col * (wall.tileSize['width']+wall.bezelSize['width'])
		y = row * (wall.tileSize['height']+wall.bezelSize['height'])
		width = wall.tileSize['width']
		height = wall.tileSize['height']
		
	print ("crop {}:{} {}x{} an image of size {}x{}".format(x,y,width,height,imageSrc.width,imageSrc.height))


	#apply wall crop offsets
	
	if col == 0:
		width -= wall.crop['left']
	elif col > 0:
		x -= wall.crop['left']
		if col == wall.numColumns-1:
			width -= wall.crop['right']
			
	if row == 0:
		height -= wall.crop['top']
	elif row > 0:
		y -= wall.crop['top']
		if row == wall.numRows-1:
			height -= wall.crop['bottom']
	 
		

	#The scaled image is sometimes not exactly the size wished (rounding problem)
	#Make sure you don't crop outside the image 
	left = x
	if left < 0:
		left = 0
	top = y
	if top < 0:
		top = 0
	right = x+width
	if right > imageSrc.width-1:
		right = imageSrc.width-1
	down = y+height
	if down > imageSrc.height-1:
		down = imageSrc.height-1
	
	print ("crop {}:{} {}x{} an image of size {}x{}".format(left,top,right,down,imageSrc.width,imageSrc.height))


	with imageSrc[left:right,top:down] as cropped:
		return saveImage(cropped, fullPath, writeFiles)
# better than: 
# 	tile = imageSrc.clone()
# 	tile.crop(x, y, width=width, height=height)
	



#Create a thumbnail image of the specified width.
def makeThumbnail(original, thumbWidth):

	print ("makeThumbnail" + str(original) + " " + str(thumbWidth))
	thumbHeight = original.height * thumbWidth/original.width

	converted = original.clone()
	conversionStr =  str(thumbWidth)+'x'+str(thumbHeight)+'^'
	converted.transform(resize=conversionStr)
	print ("makeThumbnail to "+str(id(converted)))
	return converted
		

	#We create a new image and draw the scaled version
	
#     NSImage* thumb = [[NSImage alloc] initWithSize:NSMakeSize(thumbWidth, thumbHeight)];
#     [thumb lockFocus];
#     [img drawInRect:NSMakeRect(0, 0, thumb.size.width, thumb.size.height) fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0 respectFlipped:YES hints:nil];
#     [thumb unlockFocus];
#     
#     return thumb;
# }



#Create the thumbnails according to the sizes in thumbSizes.
#For good results, the sizes should be sorted in increasing order.

def makeThumbnails(img, destDirName, numSize, thumbSizes, noexec):
	for i in range(0, numSize):
		size = thumbSizes[i]
		print("Creating thumbnail "+ str(size))
		thumb = makeThumbnail(img, size)
		if not saveImage(thumb,join(destDirName, "thumb"+str(size)+".png"), noexec):
			return False
	return True



#Create the tiles for an image, store them in the destination directory according to the tileNames array
def makeTiles(img, destDirName, wall, imageParams, noexec):
	for row in range(0,wall.numRows):
		for col in range(0, wall.numColumns):
			print("Creating tile" + str(row) + ", " + str(col) + wall.tileNames[row][col])
			tileCreated = makeTile(img, row, col, wall, imageParams, join(destDirName, wall.tileNames[row][col]+".png"), noexec) #account for origin at bottom left
			if not tileCreated:
				return False
	return True


