This is a Python version of Michel Beaudouin-Lafon's tilecutter (https://bitbucket.org/mblinsitu/tilecutter)

Wand is used to transform images (http://wand-py.org/ Copyright (c) 2015 Hong Minhee, http://dahlia.kr/ <minhee@dahlia.kr>)

Wand is a binding of ImageMagick so it requires the MagickWand library http://www.imagemagick.org/

on Ubunutu:
$ sudo apt-get install libmagickwand-dev (http://docs.wand-py.org/en/0.3.9/guide/install.html#install-imagemagick-on-debian-ubuntu)

on Mac see http://docs.wand-py.org/en/0.3.9/guide/install.html#install-imagemagick-on-mac

Using PyPI
$ apt-get install libmagickwand-dev
$ pip install Wand