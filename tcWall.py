# 
#   tcWall.py
#   tilecutter
# 
#   Created by Michel Beaudouin-Lafon on 4/7/14.
#   Ported by Olivier Gladin, SED SACLAY, INRIA on January 2015
#   Copyright (c) 2014 Michel Beaudouin-Lafon. All rights reserved.


from os.path import basename, exists, dirname, normpath, split, join, getmtime
from tcFile import *
import os
import json
from time import strptime
from calendar import timegm

from copy import deepcopy

class Wall(object):

    def __init__(self):
        #Create the default wall, used when no wall is specified
        self.name = "WILD"
        self.tileSize = {'width':2560, 'height':1600}   #tile size
        self.bezelSize = {'width':200, 'height':240}   #bezel size
        self.crop = {'top':0, 'bottom':0, 'left':0, 'right':0}
        self.numColumns = 8
        self.numRows = 4
        self.tileNames = [["a1_L", "a1_R", "b1_L", "b1_R", "c1_L", "c1_R", "d1_L", "d1_R"], \
                       ["a2_L", "a2_R", "b2_L", "b2_R", "c2_L", "c2_R", "d2_L", "d2_R"], \
                       ["a3_L", "a3_R", "b3_L", "b3_R", "c3_L", "c3_R", "d3_L", "d3_R"], \
                       ["a4_L", "a4_R", "b4_L", "b4_R", "c4_L", "c4_R", "d4_L", "d4_R"]]
        
        self.sizeWithoutBezels = {'width':self.numColumns * self.tileSize['width'], 'height':self.numRows * self.tileSize['height']} 

    
        self.sizeWithBezels = {'width':self.sizeWithoutBezels['width'] + (self.numColumns -1) * self.bezelSize['width'],\
                                  'height':self.sizeWithoutBezels['height'] + (self.numRows - 1) * self.bezelSize['height']}

    
        self.mdate = timegm(strptime("31 Dec 2999", "%d %b %Y")) 

    
    #Extract wall name from wall argument

    def getName(self, filePath):
        root = basename(filePath)
        baseName, extension = splitext(root)
        if extension.lower() == '.json':
            return baseName
        else: 
            return root


    #Lookup config file and parse wall name
    def findConfigFile(self, wname, destDir, fileName, wallName):
    
        configFile = wname
        ext = splitext(wname)[1]
        
        if not ext.lower() == '.json':
            configFile += '.json'
        
        #If the name has slashes, we're done
        if configFile.find('/') != -1:
            if exists(configFile):return configFile
            return ''
            
    
        #The name contains slashes: look up in <destDir>/configs and up
        dirName = getDestDir(destDir,dirname(fileName),basename(fileName),wallName)
        
        if not dirName.startswith("/"):
            #relative path: prefix with current directory
            cwd = os.getcwd()
            dirName = cwd + dir
            
        dirName = normpath(dirName) #was dir = [dir stringByStandardizingPath];   
    
        while (dirName != "/"): 

            tryFile = join (dirName, "configs", configFile)
            if exists(tryFile):
                return tryFile
            dirName =  split(dirName)[0]
    
        return ''


    
    #Extract data from config dictionary

    def getConfigInteger(self, configDictionary, path):
        value =  configDictionary.get(path,'')
        try:
            return int(value)
        except ValueError,e:
            print e.strerror +"\nExpected int value for " + path
    
        print "Cannot access config property "+ path
    
        return 0
    
    
    #Load a config file.
    #Return error message or nil if loaded

    def loadJSONConfig(self, fileName):
    
        #Load the content of the JSON file
        if not len(fileName) or not exists(fileName):
            print ("Cannot find file " +fileName)
            return False
        
        self.mdate = getmtime(fileName)
        
        f = open(fileName)
        if f:
            #Parse it into a dictionary
            myJson = json.load(f) 
            if myJson:
                
                tileWidth = myJson['wall']['tileSize']['width']
                tileHeight = myJson['wall']['tileSize']['height']
                self.tileSize  = {'width':tileWidth, 'height':tileHeight}
                self.bezelSize  = {'width':myJson['wall']['bezelSize']['width'], 'height':myJson['wall']['bezelSize']['height']}
                if myJson['wall'].get('crop','') != '':
                    top = myJson['wall']['crop']['top']
                    bottom = myJson['wall']['crop']['bottom']
                    right = myJson['wall']['crop']['right']
                    left = myJson['wall']['crop']['left']
                    if left >= tileWidth or right >= tileWidth:
                        print ("Wall crop left/right can't be greater than tile width")
                        return False
                    elif top >=tileHeight or bottom >= tileHeight:
                        print ("Wall crop top/bottom can't be greater than tile height")
                        return False
                    self.crop  = {'top':top,'bottom':bottom,'right':right,'left':left}
                self.numColumns = myJson['wall']['numTiles']['columns']
                self.numRows = myJson['wall']['numTiles']['rows']
  
                print ("%dx%d tiles of %dx%d pixels with bezel %dx%d pixels\n" % (self.numRows, self.numColumns, \
                       self.tileSize['width'], self.tileSize['height'], self.bezelSize['width'], self.bezelSize['height']))
    
                #Get the tile names
                self.tileNames = deepcopy(myJson['wall']['tiles']) 
                if not self.tileNames:
                    print "Cannot access config property wall.tiles"
            else:
                print ("An error occured while parsing config file  " + fileName)                
        else:
            print ("Cannot open config file " + fileName)
        
        #Transform the tile names that are of the form ["host", "name"] into "host_name"
        for row in range(0,self.numRows):
            for col in range(0,self.numColumns):
                name = self.tileNames [row] [col]
                tile = name[0]+"_"+ name[1]
                self.tileNames [row][col] = tile
    
        return True



    #Load config file
    #destDir and fileName are used to look up the config file
    #if wname is nil, lookup the WALL environment variable and,
    #if it is undefined, use the default configuration

    def load(self, wname, destDir, fileName):
    
        #Figure out the wall name (full path in wname, short name in wallname)
        if len(wname): 
            #wallname was passed on the command line
            wallName = self.getName(wname)
        else:
            #get the wall name from $WALL
            wallEnv = os.getenv("WALL")
            if len(wallEnv):
                wname = wallEnv
            else:
                wname = "WILD";
            wallName = wname;
            
        print ("Wall name: "+wallName)
    
        #Look up for the corresponding config file
        configFile = self.findConfigFile(wname, destDir, fileName, wallName)
        if not len(configFile):
            #Default to WILD if not found
            if wallName != "WILD":
                print "Could not find wall file "+ wname 
                return False
    
        #Load wall config unless we're using the default one
        if not self.loadJSONConfig(configFile):
            print "Could not load wall file %s: %s\n" + configFile
            return False

        
        self.name = wallName;
    
        #Compute overall wall size
        self.sizeWithoutBezels = {'width':self.numColumns * self.tileSize['width'], 'height':self.numRows * self.tileSize['height']} 

    
        self.sizeWithBezels = {'width':self.sizeWithoutBezels['width'] + (self.numColumns -1) * self.bezelSize['width'],\
                                  'height':self.sizeWithoutBezels['height'] + (self.numRows - 1) * self.bezelSize['height']}
    
        return True




    
    
