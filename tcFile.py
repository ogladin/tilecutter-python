# 
#   tcFile.py
#   tilecutter
# 
#   Created by Michel Beaudouin-Lafon on 4/7/14.
#   Ported by Olivier Gladin, SED SACLAY, INRIA on January 2015
#   Copyright (c) 2014 Michel Beaudouin-Lafon. All rights reserved.


from os.path import splitext,exists,join, isdir
from os import makedirs,listdir

from string import find


from tcParameters import tcImageParameters


def getDestDir(dest, dirName, fileName, wallName):
    
    #An empty destination corresponds to the current directory
    if not len(dest) or dest == "":
        dest = "."
    
    #We use dir to substitute %D in the destination directory
    if not len(dirName) or dirName == "":
        dirName = "."
    
    #Strip extension from file name
    
    if len(fileName):
        fileName = splitext(fileName)[0];
    else :
        fileName = ""
    
    #Build the destination directory by substituting %D %F %W
     
    res = dest.replace("%D",dirName)
    res = res.replace("%F",fileName)
    res = res.replace("%W",wallName)
    
    return res




#Create a directory and the path to it

def createDirPath(dirName, noExec):
    #Create the directory if needed
    
    if not noExec:
        if not exists(dirName):
            try:
                makedirs(dirName)
            except:
                return False
        
        return True
    else:
        print ("Noexec: did not try to create directory "+dirName)
        return True
    
    
    
#Return a list of paths in a directory
def dirList(dirPath):
    
    try:
        subpaths = listdir(dirPath)
    except:
        return None
    
    return subpaths


#Apply f to all files in array, from root directory dir
def mapFilesArray(dirName, subDirName, files, myFunct, recurse, wall):
    #ImageParams* defaults = [[ImageParams alloc] init];

    #fullDir is dir/subdir, or the real path if it is a symbolic link
    fullDirName = dirName
    if len(subDirName):
        fullDirName = join(dirName, subDirName)

    for currentFile in files:
        fullPathName = join(fullDirName,currentFile)
        #Skip .files
        if find(currentFile,".")==0:
            continue
        
        if exists(fullPathName) and isdir(fullPathName):
            if not recurse: continue
            #Check if the directory is marked as being skipped
            imageParams = tcImageParameters()
            defaultParams = tcImageParameters()

            imageParams = imageParams.loadImageParams(fullPathName, defaultParams)
            if not imageParams:
                print("Skipping subdirectory "+fullPathName);
                continue
                        
            #Recurse
            subpaths = dirList(fullPathName)
            if not subpaths or not mapFilesArray(dirName, join(subDirName,currentFile), subpaths, myFunct, True, wall):
                continue
        else:
            myFunct(dirName,join(subDirName,currentFile),wall)
            


    
#Apply f to the files in the directory (non recursively)
def mapFiles(dirName, myFunct, wall):
    subpaths = dirList(dirName)
    if subpaths:
        return mapFilesArray(dirName, "", subpaths, myFunct, False, wall);
    return False



#Apply f to the files in the directory (recursively),
#skipping subdirectories marked as such in tilecutter.json files

def mapFilesAndDirs(dirName, myFunct, wall):
    subpaths = dirList(dirName)
    if subpaths:
        return mapFilesArray(dirName, "", subpaths, myFunct, True, wall);
    return False
