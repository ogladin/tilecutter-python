# 
#   tilecutter.py
#   tilecutter
# 
#   Created by Michel Beaudouin-Lafon on 4/7/14.
#   Ported by Olivier Gladin, SED SACLAY, INRIA on January 2015
#   Copyright (c) 2014 Michel Beaudouin-Lafon. All rights reserved.


from tcParameters import tcParameters,tcImageParameters
from tcFile import getDestDir, createDirPath, mapFilesAndDirs, mapFiles

import tcWall

from tcImage import loadImage,isImageFile, oldestTile, oldestThumbnail, scaleToWall, makeTiles, makeThumbnails

from time import strptime
from calendar import timegm

from os.path import join, getmtime, exists, isdir, split

from sys import exit

from gc import collect

wall = None

#Thumbnails to be generated
numThumbs = 5
thumbs = [100, 200, 500, 1000, 2000]

params = None

#Process an image file
def processImageFile(dirName, fileName, wall): 
    pathName = join (dirName,fileName)
    defaultParams = tcImageParameters(params.args)
    imageParams = tcImageParameters(params.args)
    
    #Check if it's an image file
    if not isImageFile(pathName):
        #Don't warn about tilecutter.json files
        if pathName.find("tilecutter.json") != -1:
            print pathName + " is not an image file. Ignored.\n"
            return True	

    
    #Load params
    imageParams = imageParams.loadImageParams(pathName, defaultParams);
    if not imageParams:
        #The file was marked as to be skipped
        print ("Skipping "+ pathName)
        return True

    genTiles = not params.args.notiles
    genThumbs = not params.args.nothumbs

    if genTiles:
        destTileDir = getDestDir(params.args.tilesdir,dirName, fileName, wall.name)
    else:
        destTileDir = ""
    
    destThumbDir=""
    
    if genThumbs:
        destThumbDir = getDestDir(params.args.thumbsdir,dirName, fileName, wall.name)
    
    
    
    
    #Check if we need to update, so we can avoid loading and processing the image
    if params.args.update:
        #Compare dates of thumbs and tiles directories with date of image file and date of tilecutter.json file
        future = timegm(strptime("31 Dec 2999", "%d %b %Y")) #was [NSDate distantFuture], introducing the year 3K bug ;)

        updateTilesorThumbs = False
        
        try:
            imageDate = getmtime(pathName)
        except:
            imageDate = future
            
        try:
            jsonDate = getmtime(imageParams.findParamsFile(pathName))
             
        except:
            jsonDate = future
        
        if jsonDate > imageDate:
            imageDate = jsonDate
            
        if  wall.mdate > imageDate:
            imageDate = wall.mdate
        
        
        
        #imageDate holds the most recent date between the json file and the image file
        #We now compare this to the oldest tile/thumb
        
        if (genTiles):
            oldest = oldestTile(file, destTileDir, wall);
            if oldest < imageDate:
                #image is more recent
                updateTilesorThumbs = True;

        
        if (genThumbs):
            oldest = oldestThumbnail(file, destThumbDir, numThumbs, thumbs)
            

            if oldest < imageDate:
                #image is more recent
                updateTilesorThumbs = True
        
        
        if not updateTilesorThumbs:
            print("Skipping "+pathName)
            if genTiles:
                print("tiles are more recent")
            if genThumbs:
                print("thumbnails are more recent")                
                
            return True

    
    #Load image
    img = loadImage(pathName)
    if not img:
        print("Error loading "+pathName)
        return params.args.ignore
    
    
    print("Processing " + pathName)
               
    if (imageParams != defaultParams):
        print("Overriden image parameters")
        bg = imageParams.bgColor
        if bg  != defaultParams.bgColor:
            print(" bgcolor="+ bg)
        if imageParams.scaleType != defaultParams.scaleType:
            print(" scale=" + imageParams.scaleType)
        
        if imageParams.ignoreBezels != defaultParams.ignoreBezels:
            print (" bezels=" + imageParams.ignoreBezels)
    
    

    #Generate thumbnails
    if genThumbs:
        if not createDirPath(destThumbDir, params.args.noexec):
            return params.args.ignore
        if not makeThumbnails(img, destThumbDir, numThumbs, thumbs, params.args.noexec):
            print("Could not create thumbnails.")
            return params.args.ignore
    
    #Scale image
    img = scaleToWall(img, wall, imageParams);
    if not img:
        print("Error while scaling image")
        return False
    
    
    #debug
    #if not saveImage(img, join(destTileDir,"big.png"),False):
    #    print ("Problem saving debug scaled img at "+join(destTileDir,"big.png"))
    
    #run the garbage collector
    collect()
    
    
    #Generate tiles
    if genTiles:
        if not createDirPath(destTileDir, params.args.noexec):
            return params.args.ignore
        if not makeTiles(img, destTileDir, wall, imageParams, params.args.noexec):
            print("Could not create tiles.")
            return params.args.ignore
    

    
    return True


#Process a file or directory argument
def processFileOrDir(pathName):
    
    if not exists(pathName):
        print(pathName + " file not found.\n")
        return params.args.ignore
    
    
    if isdir(pathName):
        if params.args.recurse:
            return mapFilesAndDirs(pathName, processImageFile, wall)
        else:
            return mapFiles(pathName, processImageFile, wall)
        
    dirName,fileName = split(pathName)
   
    return processImageFile(dirName, fileName,wall)



def main ():
    global params, wall 
    params = tcParameters()

    print(params.args)

    #Load the wall configuration (we need a file name in case destir contains %D or %F
    if len(params.args.files):
        wall = tcWall.Wall() 
        wall.load(params.args.wallname, params.args.destdir, params.args.files[0])
    else:
        print ("missing file or directory parameter")
        exit(-1)

    for path in params.args.files:
        if not processFileOrDir(path): exit(-1)
    exit(0)


# 	print params.args.noexec
# 	
# 	print params.findParamsFile('/home/gladin/Pictures/a1')
# 	
# 	
# 	imgParam = tcImageParameters () 
# 	imgParam.loadImageParams('/home/gladin/_tilecutter.json',"")
	
	
if __name__ == '__main__':
	main()