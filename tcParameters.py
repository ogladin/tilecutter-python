# 
#   tcParameters.py
#   tilecutter
# 
#   Created by Michel Beaudouin-Lafon on 4/7/14.
#   Ported by Olivier Gladin, SED SACLAY, INRIA on January 2015
#   Copyright (c) 2014 Michel Beaudouin-Lafon. All rights reserved.

import argparse
from os.path import dirname,exists,basename,splitext,join

from utils import Enum
ScaleType = Enum(['ScaleFill', 'ScaleFit', 'ScaleMap', 'ScaleNone'])

import json

class tcImageParameters(object):
    def __init__(self, args=None):
        if args:
            #set command line parameters as default
            self.bgColor = args.bgcolor
   
            d = {'fill':ScaleType.ScaleFill, 'fit':ScaleType.ScaleFit,'map':ScaleType.ScaleMap,'non':ScaleType.ScaleNone,'none':ScaleType.ScaleNone}
            if len(args.scale) and args.scale.lower() in d:   
                self.scaleType = d[args.scale.lower()]
            else:
                self.scaleType = ScaleType.ScaleFit    
                
            self.ignoreBezels = args.nobezels
                
            if args.rotateR:
                self.rotate = -90
            elif args.rotateL: 
                self.rotate = 90
            else:
                r = {'left':90, 'right':-90}
                if len(args.rotate) and args.rotate.lower() in r:   
                    self.rotate = d[args.rotate.lower()]
                else:
                    self.rotate = 0     
        else:
            self.bgColor = "black"
            self.scaleType = ScaleType.ScaleFit
            self.rotate = 0;
            self.ignoreBezels = False;
        
    #Extract parameters from config and copy them to params.
    #Return false if 'skip' is true
    def setImageParams(self,imageParams, configDictionary):
        skip = configDictionary.get('skip','')
        if skip in ['true','yes']: return False

        colorName = configDictionary.get('bgcolor','')
        if colorName:
            imageParams.bgColor = colorName
            
        scale = configDictionary.get('scale','')
        scale = scale.lower()    
        d = {'fill':ScaleType.ScaleFill, 'fit':ScaleType.ScaleFit,'map':ScaleType.ScaleMap,'non':ScaleType.ScaleNone,'none':ScaleType.ScaleNone}
        if len(scale) and scale in d:   
            imageParams.scaleType = d[scale]
        
        
        bezels = configDictionary.get('bezels','')
        if bezels in ['true','yes']: imageParams.ignoreBezels = False
        if bezels in ['false','no']: imageParams.ignoreBezels = True
        
        
        rotate = configDictionary.get('rotate','')

        rotate = rotate.lower()    
        d = {'left':90, 'right':-90}
        if len(rotate) and rotate in d:   
            imageParams.rotate = d[rotate]
        
        return True
            
    # Return the name of the params file, if the file exists, nil otherwise
    # We look up tilecutter.json, then _tilecutter.json, finally .tilecutter.json
    def findParamsFile(self,fileName):
        
        baseDir = dirname(fileName)
        configFile = baseDir+'/tilecutter.json'
        if (exists(configFile)): return configFile
        configFile = baseDir+'/_tilecutter.json' 
        if (exists(configFile)): return configFile
        configFile = baseDir+'/.tilecutter.json' 
        if (exists(configFile)): return configFile
        return ''
    
                
    #Load the parameters from 'tilecutter.json' if it exists, otherwise from the defaults.
    #If the parameters state to skip the file, return nil.
    def loadImageParams(self,path, defaultImageParams):
   
   
        paramsFile = self.findParamsFile(path)
         
        #Load the content of the JSON file
        if not len(paramsFile) or not exists(paramsFile): return defaultImageParams
        
        f = open(paramsFile)
        
        if not f: return defaultImageParams
        
        fcontent = f.read()
        
        if not len(fcontent): return defaultImageParams
        
        #Parse it into a dictionary
        myJson = json.loads(fcontent)
        
        #Extract the directory-level fields
        imageParams = defaultImageParams
        
        if not self.setImageParams(imageParams,myJson): return None
        
        #Extract the file-level fields
        #Look for file name with, then without extension
        #(This is for situations where several files have the same root name but different extensions)
        
        fileName = basename(path)
        rootName = splitext(fileName)
        
        #Try full name
        imageConfig = myJson.get(fileName,None)
        if not imageConfig:
            #Try name without extensions
            imageConfig = myJson.get(rootName,None)

        if not imageConfig:
            #Not found: return current params
            return imageParams

            
        #Special case where the value is "skip"
        if isinstance(imageConfig, basestring) and imageConfig == 'skip':
            return None
    
        #General case where the value is a set of parameters
        if isinstance(imageConfig,dict):
            if not self.setImageParams(imageParams, imageConfig): return None
    
        return imageParams
        
        
        #print myJson['bgcolor']
        
        #print json.dumps(myJson, sort_keys=True, indent=4, separators=(',', ': '))                    



class tcParameters(object):
    def __init__(self):
        parser = argparse.ArgumentParser(
        description='Image cutter for WildOS',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=    '''
Wall name defaults to $WALL if defined, otherwise to WILD. A .json suffix
is added if not present. If the name does not contain slashes, it is looked
up in <destDir>/configs, <destDir>/../configs, etc.

The default destination directory is the root file name of the image 
(e.g., foo/ for foo.png). Tiles are stored by default in <destDir>/tiles_byhost,
thumbnails in <destDir>/thumbs.These directories can be overriden with the
corresponding options. In this case %D is replaced with the file's directory,
%F with the file's root name and %W with the wall name.

Background color, scale, rotation and bezels can be overriden by a 
'tilecutter.json' file in the file's directory.The file can also be called 
'_tilecutter.json' or '.tilecutter.json'. They are looked up in this order.

Use entries "bgcolor": "color name or hex value", "scale": "none|fit|fill|map",
"bezels": "yes|no" "rotate": "left|right" or "skip": "yes|no". The latter is
used to ignore all the files in the directory.

Per-file properties can be specified by using an entry with the root name of
the file. The special case "filename": "skip" is a shortcut for "filename":
{\"skip\": \"yes\"}. Skipping directories in this way is effective only with
the -r option'''
    )

        parser.add_argument('-n', '-noexec', action='store_true', dest='noexec', help='process images but do not write any file (for testing)')
        parser.add_argument('-r', '-recursive', action='store_true', dest='recurse', help='recurse through file arguments that are directories')
        parser.add_argument('-u', '-update', action='store_true', dest='update', help='only update existing tiles and thumbnails if they are older than the source image')
        parser.add_argument('-i', '-ignore', action='store_true', dest='ignore', help='continue processing in case of an error')
        parser.add_argument('-w', '-wall', action='store', dest='wallname', default='wild',help='wall name (see below)')
        parser.add_argument('-d', '-destdir', action='store', dest='destdir',default='',  help='destination directory (see below)')
        parser.add_argument('-T', '-tilesdir', action='store', dest='tilesdir', default='', help='override default tiles directory')
        parser.add_argument('-t', '-thumbsdir', action='store', dest='thumbsdir', default='', help='override default thumbnails directory')
        parser.add_argument('-1', '-no-tiles', action='store_true', dest='notiles', help='do not generate tiles')
        parser.add_argument('-b', '-no-bezels', action='store_true', dest='nobezels', help='ignore bezels when generating tiles')
        parser.add_argument('-2', '-no-thumbs', action='store_true', dest='nothumbs', help='do not generate thumbnails')
        parser.add_argument('-c', '-bgcolor', action='store', dest='bgcolor', default='black',help='set the background color if the image does not cover the wall. \'rgb(255, 255, 255)\', \'#fff\', \'white\'')
        parser.add_argument('-s', '-scale', choices=['none', 'fit', 'fill', 'map'], dest='scale', default='fit',
            help=''''s|-scale none|fit|fill|map: how to scale the image to the wall size. 
                none: do not scale image. Center it and fill with bg color.
                fit (default):  scale image so that it is entirely visible. Does not crop the original image 
                fill: scale image so that it covers the wall. Crops the original image if needed.  
                map:  scale the image separately in x and y to fill the wall. Results in a distorted image''')
        
        parser.add_argument('-O', '-rotate', choices=['left', 'right'], dest='rotate', default='',
            help='rotate the image 90 degrees left (anticlockwise) or right (clockwise)')
        parser.add_argument('-R', action='store_true', dest='rotateR', help='rotate right')
        parser.add_argument('-L', action='store_true', dest='rotateL', help='rotate left')
        parser.add_argument('files', nargs='*')
        
        
        self.args = parser.parse_args()
        
        
        #Default directories
        if not len(self.args.destdir):
            self.args.destdir = "%D"
        if not len(self.args.tilesdir):
            self.args.tilesdir = join (self.args.destdir, "%F/tiles_byhost")
        if not len(self.args.thumbsdir):
            self.args.thumbsdir = join (self.args.destdir, "%F/thumbs")

        
     
    

