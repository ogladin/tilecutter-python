# 
#   utils.py
#   tilecutter
# 
#   Created by Michel Beaudouin-Lafon on 4/7/14.
#   Ported by Olivier Gladin, SED SACLAY, INRIA on January 2015
#   Copyright (c) 2014 Michel Beaudouin-Lafon. All rights reserved.

class Enum(set):
    def __getattr__(self, name):
        if name in self:
            return name
        raise AttributeError
    
    